package com.Task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Вивести номери дверей в тому порядку,
 * в якому слід їх відкривати герою,
 * щоб залишитися в живих, якщо таке можливо.
 *
 * @author Nazarii Yaremchuk
 * 25.11.2019
 */

public class DequeTest {

    private static Logger logger = LogManager.getLogger(DequeTest.class);

    private Deque<Integer> deque;

    private DequeTest() {
        deque = new ArrayDeque<>();
    }


    public static void main(String[] args) {
        DequeTest dequeTest = new DequeTest();
        for (int i = 0; i < 10; i++) {
            dequeTest.deque.addFirst(i);
            dequeTest.deque.addLast(i);
        }
        logger.info(dequeTest.deque.toString());
    }
}
