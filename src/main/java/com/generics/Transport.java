package com.generics;

import java.util.ArrayList;
import java.util.List;

public class Transport<T> {
    private List<? super T> transport = new ArrayList<>();

    public void addElement(T element) {
        transport.add(element);
    }

    public int getPrice() {  ///////
        return transport.size();
    }

    public T getElements(int i) {
        return (T) transport.get(i);
    }
}
