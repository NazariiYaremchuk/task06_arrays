package com.generics;

public class Auto {
    protected String name;

    Auto() {

    }

    Auto(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name + " ";
    }
}