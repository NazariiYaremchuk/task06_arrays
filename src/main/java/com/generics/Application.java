package com.generics;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Queue;

public class Application {
    public static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
/**
 * Write your generic class – container with units.
 * You can put and get units from container.
 * Try to implement it using wildcards.
 */
        Transport<Auto> transport = new Transport();
        transport.addElement(new Car("Mersedes"));
        transport.addElement(new Bus("Electron"));
        transport.addElement(new Truck("Volvo"));

        for (int i = 0; i < transport.getPrice(); i++) {
            logger.info(transport.getElements(i));
        }

        //  Write your PriorityQueue class using generics.
        Queue<Integer> myPriorityQueue = new MyPriorityQueue<Integer>((o1, o2) -> o2 - o1);
        myPriorityQueue.add(5);
        myPriorityQueue.add(12);
        myPriorityQueue.add(4);
        myPriorityQueue.add(45);
        myPriorityQueue.add(36);
        myPriorityQueue.add(68);
        myPriorityQueue.add(5);
        myPriorityQueue.add(4);
        myPriorityQueue.add(45);
        myPriorityQueue.add(36);

        while (!myPriorityQueue.isEmpty()) {
            logger.info(myPriorityQueue.poll());
        }

        //  Try how you can add string into List<Integers>.
//        List<Integer> myList = new ArrayList<>();
//        myList.add("Hello");


    }
}