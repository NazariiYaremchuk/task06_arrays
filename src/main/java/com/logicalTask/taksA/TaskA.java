package com.logicalTask.taksA;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

/**
 * Дано два масиви. Сформувати третій масив,
 * що складається з тих елементів, які:
 * а) присутні в обох масивах;
 * б) присутні тільки в одному з масивів.
 */

public class TaskA {
    public static Logger logger = LogManager.getLogger(TaskA.class);
    private int[] mas1;
    private int[] mas2;

    TaskA() {
        mas1 = new int[20];
        mas2 = new int[30];

        for (int i = 0; i < mas1.length; i++) {
            mas1[i] = (int) (Math.random() * 100);
        }
        for (int i = 0; i < mas2.length; i++) {
            mas2[i] = (int) (Math.random() * 100);
        }

    }


    private boolean isInMasive(int element, int[] mas3) {
        for (int i : mas3) {
            if (element == i) {
                return true;
            }
        }
        return false;
    }


    private boolean isInBothMasivs(int element) {
        if (isInMasive(element, mas1) && isInMasive(element, mas2)) {
            return true;
        } else return false;
    }

    private int[] getCommonElementsMasuv(int[] mas1, int[] mas2) {
        int counter = 0;
        for (int i : mas1) {
            if (isInMasive(i, mas2)) counter++;
        }

        int[] commonElements = new int[counter];

        for (int j = 0, elem = 0, commoncounter = 0; j < mas1.length; elem = mas1[j], j++) {

            if (isInMasive(elem, mas2)) {
                commonElements[commoncounter] = elem;
                commoncounter++;
            }

        }

        return commonElements;
    }

    public void view() {
        logger.info((Arrays.toString(getCommonElementsMasuv(mas1, mas2))));
    }


    public static void main(String[] args) {
        TaskA taskA = new TaskA();
        logger.info("First masive: " + Arrays.toString(taskA.mas1));
        logger.info("Second masive: " + Arrays.toString(taskA.mas2));
        taskA.view();


    }

}
