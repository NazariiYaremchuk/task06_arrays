package com.logicalTask.taskB;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Видалити в масиві всі числа,
 * які повторюються більше двох разів.
 */

public class TaskB {
    private static Logger logger = LogManager.getLogger(TaskB.class);
    private static int[] mas4;

    TaskB() {
        mas4 = new int[10];

        for (int j = 0; j < mas4.length; j++) {
            mas4[j] = (int) (Math.random() * 100);
        }
    }

    private int isRepeated(int element1) {
        int counter = 0;
        for (int i : mas4) {
            if (i == element1) {
                counter++;
            }
        }
        return counter;
    }

    private boolean isInMasuv(int element, int[] mas) {
        for (int i : mas) {
            if (element == i) return true;
        }
        return false;
    }

    private int counter = 0;
    private int allCounter = 0;

    private int[] getPopularNumbers() {
        int length = 0;

        for (int value : mas4) {
            if (isRepeated(value) >= 3) length += isRepeated(value);
        }
        int[] popularNumbers = new int[length / 3];
        for (int value : mas4) {
            if (isRepeated(value) >= 3) {
                allCounter++;
                if (!isInMasuv(value, popularNumbers)) {
                    popularNumbers[counter] = value;
                    counter++;
                }

            }
        }
        return popularNumbers;
    }

    private void removePopularNumbers() {
        int[] popularNumbers = getPopularNumbers();
        int[] newMasuv = new int[mas4.length - allCounter];
        for (int i = 0, j = 0; i < mas4.length; i++) {
            if (!isInMasuv(mas4[i], popularNumbers)) {
                newMasuv[j] = mas4[i];
                j++;
            }
        }
        mas4 = newMasuv;
    }


    public static void main(String[] args) {
        TaskB taskB = new TaskB();
        logger.info(Arrays.toString(mas4));

        taskB.removePopularNumbers();
        logger.info(Arrays.toString(mas4));
    }

}
