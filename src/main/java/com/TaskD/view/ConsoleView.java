package com.TaskD.view;

import com.TaskD.controller.Game;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * D. Герой комп'ютерної гри, що володіє силою в 25 балів,
 * знаходиться в круглому залі, з якого ведуть 10 закритих дверей.
 * За кожними дверима героя чекає або магічний артефакт,
 * що дарує силу від 10 до 80 балів,
 * або монстр, який має силу від 5 до 100 балів,
 * з яким герою потрібно битися. Битву виграє персонаж,
 * що володіє найбільшою силою; якщо сили рівні, перемагає герой.
 *
 * @author Nazarii Yaremchuk
 * 25.11.2019
 */

public class ConsoleView {
    private static Logger logger = LogManager.getLogger(ConsoleView.class);
    Game game = new Game();

    public void view() {
        logger.info(game.getInfo());
        viewDeadDoors();
        viewSurviveDoors();
    }

    private void viewDeadDoors() {
        logger.info("Doors of dead number: " + game.calculateDeadDoors());
    }

    private void viewSurviveDoors() {
        logger.info(game.surviveDoorsArray());
    }
}
