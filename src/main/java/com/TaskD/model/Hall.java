package com.TaskD.model;

import java.util.*;

/**
 * D. Герой комп'ютерної гри, що володіє силою в 25 балів,
 * знаходиться в круглому залі, з якого ведуть 10 закритих дверей.
 * За кожними дверима героя чекає або магічний артефакт,
 * що дарує силу від 10 до 80 балів,
 * або монстр, який має силу від 5 до 100 балів,
 * з яким герою потрібно битися. Битву виграє персонаж,
 * що володіє найбільшою силою; якщо сили рівні, перемагає герой.
 *
 * @author Nazarii Yaremchuk
 * 25.11.2019
 */

public class Hall {
    private final int DOORS_SIZE = 10;
    List<Door> doors;
    Hero hero;

    public Hall() {
        doors = new LinkedList<>();
        hero = new Hero();
        for (int i = 0; i < DOORS_SIZE; i++) {
            doors.add(new Door());
        }
    }

    public String getInfo() {
        StringBuilder sb = new StringBuilder("\n Hidden object   HP\n");

        for (Door d : doors) {
            sb.append(d.toString() + "\n");
        }
        return sb.toString();
    }

    public int calculateDoorsOfDead(int summ, Iterator<Door> it) {
        if (hero.getHp() + it.next().open() < 0) summ++;
        if (!it.hasNext()) return summ;
        summ = calculateDoorsOfDead(summ, it);
        return summ;
    }

    public List<Door> getDoors() {
        return doors;
    }

    public String surviveDoors() {
        int[] doorNumbers = new int[DOORS_SIZE];
        for (int i = 0, j = DOORS_SIZE - 1, count = 0; count < doors.size(); count++) {
            if (doors.get(count).open() < 0) {
                doorNumbers[j] = count;
                j--;
            } else {
                doorNumbers[i] = count;
                i++;
            }
        }
        for (int d : doorNumbers) {
            hero.updateHP(doors.get(d));
        }
        if (hero.getHp() > 0) return "You can survive with next door order: " + Arrays.toString(doorNumbers);
        else return "Sorry, you must die(";

    }
}