package com.TaskD.model;

/**
 * D. Герой комп'ютерної гри, що володіє силою в 25 балів,
 * знаходиться в круглому залі, з якого ведуть 10 закритих дверей.
 * За кожними дверима героя чекає або магічний артефакт,
 * що дарує силу від 10 до 80 балів,
 * або монстр, який має силу від 5 до 100 балів,
 * з яким герою потрібно битися. Битву виграє персонаж,
 * що володіє найбільшою силою; якщо сили рівні, перемагає герой.
 *
 * @author Nazarii Yaremchuk
 * 25.11.2019
 */

public abstract class Hidden {
    boolean isActive;
    int hp;
    private final int MIN_HP;
    private final int MAX_HP;

    Hidden(int min_hp, int max_hp) {
        MAX_HP = max_hp;
        MIN_HP = min_hp;
        hp = MIN_HP + (int) (Math.random() * (MAX_HP - MIN_HP));
        isActive = true;
    }

    public abstract int interuct();

    public void setActive(boolean active) {
        isActive = active;
    }
}
