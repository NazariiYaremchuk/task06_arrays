package com.TaskD.model;

/**
 * D. Герой комп'ютерної гри, що володіє силою в 25 балів,
 * знаходиться в круглому залі, з якого ведуть 10 закритих дверей.
 * За кожними дверима героя чекає або магічний артефакт,
 * що дарує силу від 10 до 80 балів,
 * або монстр, який має силу від 5 до 100 балів,
 * з яким герою потрібно битися. Битву виграє персонаж,
 * що володіє найбільшою силою; якщо сили рівні, перемагає герой.
 *
 * @author Nazarii Yaremchuk
 * 25.11.2019
 */

public class Door {
    private Hidden hidden;

    Door() {
        if (Math.random() > 0.5) hidden = new Monster();
        else hidden = new Artifact();
    }

    int open() {
        return hidden.interuct();
    }

    @Override
    public String toString() {
        return String.format(" %10s\t\t %d",
                hidden.getClass().getSimpleName(), hidden.hp);

    }
}
